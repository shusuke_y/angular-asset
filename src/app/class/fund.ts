export class Fund {
  fundNo: string;
  fundNm: string;
  companyCd: string;
  companyNm: string;
  startDate: Date;
  maturityDate: Date;
  firstPaymentDate: Date;
  paymentInterval: number;
  amount: number;
  yield: number;
  interestRate: number;
  currency: string;

  companys: string[] = [
    'CCR', 'OWB', 'SBI', 'CBK', 'CCR'
  ];
  currencys: string[] = [
    'JPY', 'USD', 'EUR', 'GBP', 'RUB', 'NGN'
  ];
}
