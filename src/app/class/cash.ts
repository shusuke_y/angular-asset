export class Cash {
  fundNo: string;
  receiveDate: Date;
  cashFlowType: string;
  receiveAmount: number;
  actualReceiveAmount: number;
  profitPreTax: number;
  actualProfitPreTax: number;
  profitAmount: number;
  actualProfitAmount: number;
  companyCd: string;

  maturity = [
    { id: '1', name: '償還前' },
    { id: '0', name: '償還済含む' },
  ];
}
