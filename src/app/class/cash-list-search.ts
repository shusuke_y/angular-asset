export class CashListSearch {
  fundNo: string;
  maturityFlg = '1';
  receiveDate: string;
  cashFlowType: string;
}
