import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {NumberInputPipe} from '../pipes/number-input.pipe';

import { Fund } from '../class/fund';
import { FundService } from '../service/fund.service';

@Component({
  selector: 'app-fund-update',
  templateUrl: './fund-update.component.html',
  styleUrls: ['./fund-update.component.css']
})
export class FundUpdateComponent implements OnInit {

  title = 'ファンド編集';
  fund: Fund;

  constructor(
    private fundService: FundService,
    private route: ActivatedRoute,
    private location: Location,
    private numberInputPipe: NumberInputPipe,
  ) { }

  ngOnInit(): void {
    const fundNo: string = this.route.snapshot.paramMap.get('id');
    this.fundService.getFund(fundNo).subscribe(fund => this.fund = fund.asset);
  }

  update(): void {
    this.fund.amount = Number(this.numberInputPipe.parse(this.fund.amount.toString()));
    this.fundService.updateFund(this.fund).subscribe(
      () => {
        this.goBack();
      }, error => {
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
