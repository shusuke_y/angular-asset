import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Fund } from '../class/fund';
import { FundService } from '../service/fund.service';

@Component({
  selector: 'app-fund-detail.component',
  templateUrl: './fund-detail.component.html',
  styleUrls: ['./fund-detail.component.css']
})
export class FundDetailComponent implements OnInit {
  title = 'ファンド詳細';
  fund: Fund;

  constructor(
    private fundService: FundService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    const fundNo: string = this.route.snapshot.paramMap.get('id');
    this.fundService.getFund(fundNo).subscribe(fund => this.fund = fund.asset);
  }

  delete(): void {
    if (confirm('削除してもいいですか？')) {
      this.fundService.deleteFund(this.fund.fundNo).subscribe(() => this.router.navigateByUrl('/funds'));
    }
  }

  goBack(): void {
    this.location.back();
  }

}
