import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FundListComponent } from './fund-list/fund.list';
import { FundAddComponent } from './fund-add/fund-add.component';
import { FundDetailComponent } from './fund-detail/fund-detail.component';
import { FundUpdateComponent } from './fund-update/fund-update.component';
import { CashListComponent } from './cash-list/cash-list.component';
import { CashUpdateComponent } from './cash-update/cash-update.component';
import { CashSumComponent } from './cash-sum/cash-sum.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'funds',
    pathMatch: 'full'
  },
  {
    path: 'funds',
    component: FundListComponent
  },
  {
    path: 'fund/add',
    component: FundAddComponent
  },
  {
    path: 'fund/detail/:id',
    component: FundDetailComponent
  },
  {
    path: 'fund/update/:id',
    component: FundUpdateComponent
  },
  {
    path: 'cash',
    component: CashListComponent
  },
  {
    path: 'cash/list/:id',
    component: CashListComponent
  },
  {
    path: 'cash/update',
    component: CashUpdateComponent
  },
  {
    path: 'cash/sum',
    component: CashSumComponent
  },
  {
    path: 'cash/sum/:id',
    component: CashSumComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
