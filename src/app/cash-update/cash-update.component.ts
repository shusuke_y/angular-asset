import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {NumberInputPipe} from '../pipes/number-input.pipe';

import { Cash } from '../class/cash';
import { CashListSearch } from '../class/cash-list-search';
import { CashService } from '../service/cash.service';

@Component({
  selector: 'app-cash-update',
  templateUrl: './cash-update.component.html',
  styleUrls: ['./cash-update.component.css']
})
export class CashUpdateComponent implements OnInit {

  title = 'キャッシュフロー編集';
  cash: Cash;
  cashListSearch: CashListSearch = new CashListSearch();
  cashFlowTypeName =
    { 1: '償還'
    , 2: '利息'
    , 3: '償還＆利息'
    };

  constructor(
    private cashService: CashService,
    private route: ActivatedRoute,
    private location: Location,
    private numberInputPipe: NumberInputPipe,
  ) { }

  ngOnInit(): void {
    this.cashListSearch.fundNo = this.route.snapshot.queryParamMap.get('fundNo');
    this.cashListSearch.receiveDate = this.route.snapshot.queryParamMap.get('receiveDate').toString().replace(/\//g, '-');
    this.cashListSearch.cashFlowType = this.route.snapshot.queryParamMap.get('cashFlowType');
    this.cashService.getCashKey(this.cashListSearch).subscribe(cashKey => this.cash = cashKey.cash);
  }

  update(): void {
    // TODO リファクタ対象
    this.cash.receiveAmount = Number(this.numberInputPipe.parse(this.cash.receiveAmount.toString()));
    if (this.cash.actualReceiveAmount !== null) {
      this.cash.actualReceiveAmount = Number(this.numberInputPipe.parse(this.cash.actualReceiveAmount.toString()));
    }
    if (this.cash.profitPreTax !== null) {
      this.cash.profitPreTax = Number(this.numberInputPipe.parse(this.cash.profitPreTax.toString()));
    }
    if (this.cash.actualProfitPreTax !== null) {
      this.cash.actualProfitPreTax = Number(this.numberInputPipe.parse(this.cash.actualProfitPreTax.toString()));
    }
    if (this.cash.profitAmount !== null) {
      this.cash.profitAmount = Number(this.numberInputPipe.parse(this.cash.profitAmount.toString()));
    }
    if (this.cash.actualProfitAmount !== null) {
      this.cash.actualProfitAmount = Number(this.numberInputPipe.parse(this.cash.actualProfitAmount.toString()));
    }

    this.cashService.updateCash(this.cash).subscribe(
      () => {
        this.goBack();
      }, error => {
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
