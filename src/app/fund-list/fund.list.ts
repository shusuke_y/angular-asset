import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Fund } from '../class/fund';
import { FundSearch } from '../class/fund-search';
import { FundService } from '../service/fund.service';

@Component({
  selector: 'app-fund-list',
  templateUrl: './fund.list.html',
  styleUrls: ['./fund.list.css']
})
export class FundListComponent implements OnInit {
  title = '購入ファンド一覧';
  fundSearch: FundSearch = new FundSearch();
  fund: Fund = new Fund();
  displayedColumns = ['fundNo', 'fundNm', 'companyCd', 'startDate', 'firstPaymentDate'
                      , 'maturityDate', 'amount', 'yield'];
  dataSource: MatTableDataSource<Fund>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private fundService: FundService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
    const today = new Date();
    this.fundSearch.maturityStart = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2)
                                      + '-' +  ('0' + today.getDate()).slice(-2);
    this.findFund();
  }

  search(): void {
    this.findFund();
  }

  findFund(): void {
    this.fundService.getFunds(this.fundSearch).subscribe(funds => {
      this.dataSource = new MatTableDataSource(funds.assets);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  navigate(fund: Fund) {
    this.router.navigate(['/fund/detail', fund.fundNo], { relativeTo: this.route });
  }

}
