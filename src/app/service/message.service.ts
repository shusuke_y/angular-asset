import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: string[] = []; // メッセージを代入する変数を定義

  constructor() { }

  add(message: string) {// 引数のmessageをmessagesにpushで代入
    this.messages = [];
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }

}
