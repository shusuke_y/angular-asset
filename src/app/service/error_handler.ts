import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {MessageService} from './message.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private messageService: MessageService) { }

  /**
   * 失敗したHttp操作を処理します。
   * アプリを持続させます。
   * @param operation - 失敗した操作の名前
   * @param result - observableな結果として返す任意の値
   */
  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: リモート上のロギング基盤にエラーを送信する
      console.error(error); // かわりにconsoleに出力

      // メッセージにエラーメッセージを格納する
      this.log(`${operation} failed: ${error.error}`);

      // エラーを返却し、component側でハンドリングする
      return throwError('エラー発生');
    };
  }

  // 追加(メッセージを代入する関数を作成(あとで使う)) TODO：エラー時は背景色を変更する
  private log(message: string) {
    this.messageService.add(`${message}`);
  }

}
