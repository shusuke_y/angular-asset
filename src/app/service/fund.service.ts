import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Observable, throwError } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {MessageService} from './message.service';
import {ErrorHandlerService} from './error_handler';

import { Fund } from '../class/fund';

@Injectable()
export class FundService {

  fundsGetUrl = 'http://localhost:8080/fund';
  fundAddUrl = 'http://localhost:8080/fund/add';
  fundUpdUrl = 'http://localhost:8080/fund/update';
  fundDelUrl = 'http://localhost:8080/fund/delete/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'charset': 'UTF-8'
    })
  };

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandlerService: ErrorHandlerService
            ) { }

  getFunds(fundSearch: any): Observable<any> {
    const url = `${this.fundsGetUrl}`;
    return this.http.get<Fund>(url, {params: fundSearch}).pipe(
      // tap(_ => this.messageService.clear()),
      catchError(this.errorHandlerService.handleError<Fund>(`getFund`))
    );
  }

  getFund(fundNo: string): Observable<any> {
    const url = `${this.fundsGetUrl}/${fundNo}`;
    return this.http.get<Fund>(url).pipe(
      // tap(_ => this.messageService.clear()),
      catchError(this.errorHandlerService.handleError<Fund>(`getFund id=${fundNo}`))
    );
  }

  createFund(fund: Fund): Observable<any> {
    return this.http.post(this.fundAddUrl, fund, this.httpOptions).pipe(
      tap(_ => this.log(`ファンドを登録しました。 ファンド名  =${fund.fundNm}`)),
      catchError(this.errorHandlerService.handleError<any>('createFund'))
    );
  }

  updateFund(fund: Fund): Observable<any> {
    return this.http.post(this.fundUpdUrl, fund, this.httpOptions).pipe(
      tap(_ => this.log(`ファンドを編集しました。 fundNo=${fund.fundNo}`)),
      catchError(this.errorHandlerService.handleError<any>('updateFund'))
    );
  }

  deleteFund(fundNo: string): Observable<any> {
    return this.http.post(this.fundDelUrl + fundNo, null).pipe(
      tap(_ => this.log(`ファンドを削除しました。 fundNo=${fundNo}`)),
      catchError(this.errorHandlerService.handleError<any>('deleteFund'))
    );
  }

  // 追加(メッセージを代入する関数を作成(あとで使う)) TODO：エラー時は背景色を変更する
  private log(message: string) {
    this.messageService.add(`${message}`);
  }

}
