import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {MessageService} from './message.service';
import {ErrorHandlerService} from './error_handler';

import { Cash } from '../class/cash';

@Injectable()
export class CashService {

  cashGetUrl = 'http://localhost:8080/cash';
  cashSumUrl = 'http://localhost:8080/cash/sum';
  cashUpdUrl = 'http://localhost:8080/cash/update';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'charset': 'UTF-8'
    })
  };

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandlerService: ErrorHandlerService
            ) { }

  getCashList(cashListSearch: any): Observable<any> {
    const url = `${this.cashGetUrl}`;
    return this.http.get<Cash>(url, {params: cashListSearch}).pipe(
      // tap(_ => this.messageService.clear()),
      catchError(this.errorHandlerService.handleError<Cash>(`getCashList`))
    );
  }

  getCashKey(cashListSearch: any): Observable<any> {
    const url = `${this.cashUpdUrl}`;
    return this.http.get<Cash>(url, {params: cashListSearch}).pipe(
      // tap(_ => this.messageService.clear()),
      catchError(this.errorHandlerService.handleError<Cash>(`getCashKey key=${cashListSearch}`))
    );
  }

  getCashSum(year: string, fundNo: string): Observable<any> {
    const url = `${this.cashSumUrl}`;
    return this.http.get<Cash>(url, {params: new HttpParams().set('year', year).set('fundNo', fundNo)}).pipe(
      tap(_ => this.messageService.clear()),
      catchError(this.errorHandlerService.handleError<Cash>(`getCash year=${year} fundNo=${fundNo}`))
    );
  }

  updateCash(cash: Cash): Observable<any> {
    return this.http.post(this.cashUpdUrl, cash, this.httpOptions).pipe(
      tap(_ => this.log(`キャッシュフローを編集しました。
        fundNo=${cash.fundNo} receiveDate=${cash.receiveDate} cashFlowType=${cash.cashFlowType}`)),
      catchError(this.errorHandlerService.handleError<any>('updateFund'))
    );
  }

  // 追加(メッセージを代入する関数を作成(あとで使う)) TODO：エラー時は背景色を変更する
  private log(message: string) {
    this.messageService.add(`${message}`);
  }

}
