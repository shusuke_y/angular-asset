import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {NumberInputPipe} from '../pipes/number-input.pipe';

import { Fund } from '../class/fund';
import { FundService } from '../service/fund.service';

@Component({
  selector: 'app-fund-add',
  templateUrl: './fund-add.component.html',
  styleUrls: ['./fund-add.component.css']
})
export class FundAddComponent implements OnInit {

  title = 'ファンド追加';
  fund: Fund = new Fund();

  constructor(
    private fundService: FundService,
    private location: Location,
    private numberInputPipe: NumberInputPipe,
  ) { }

  ngOnInit(): void {
    this.fund.startDate = new Date();
  }

  add(): void {
    this.fund.amount = Number(this.numberInputPipe.parse(this.fund.amount.toString()));
    this.fundService.createFund(this.fund).subscribe(
      () => {
        this.goBack();
      }, error => {
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
