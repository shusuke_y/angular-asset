import { BrowserModule } from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { FundService } from './service/fund.service';
import { CashService } from './service/cash.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FundListComponent } from './fund-list/fund.list';
import { FundAddComponent } from './fund-add/fund-add.component';
import { FundDetailComponent } from './fund-detail/fund-detail.component';
import { FundUpdateComponent } from './fund-update/fund-update.component';
import localeJa from '@angular/common/locales/ja';
import { registerLocaleData } from '@angular/common';
import { NumberInputPipe } from './pipes/number-input.pipe';
import { NumberInputDirective } from './directives/number-input.directive';
import { CashListComponent } from './cash-list/cash-list.component';
import { CashSumComponent } from './cash-sum/cash-sum.component';
import { MessageComponent } from './message/message.component';
import { CashUpdateComponent } from './cash-update/cash-update.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';


registerLocaleData(localeJa);

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  declarations: [
    AppComponent,
    FundDetailComponent,
    FundListComponent,
    FundUpdateComponent,
    FundAddComponent,
    NumberInputPipe,
    NumberInputDirective,
    CashListComponent,
    CashSumComponent,
    MessageComponent,
    CashUpdateComponent
  ],
  providers: [FundService, CashService, {provide: LOCALE_ID, useValue: 'ja-JP'}, NumberInputPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
