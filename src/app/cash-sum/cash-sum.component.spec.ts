import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashSumComponent } from './cash-sum.component';

describe('CashSumComponent', () => {
  let component: CashSumComponent;
  let fixture: ComponentFixture<CashSumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashSumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashSumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
