import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CashSumSearch } from '../class/cash-sum-search';
import { CashService } from '../service/cash.service';

@Component({
  selector: 'app-cash-sum',
  templateUrl: './cash-sum.component.html',
  styleUrls: ['./cash-sum.component.css']
})
export class CashSumComponent implements OnInit {
  title = '年間キャッシュフロー';
  cashSum: [];
  cashSumSearch: CashSumSearch = new CashSumSearch();
  diffSum: number;

  constructor(
    private cashService: CashService ,
    private route: ActivatedRoute
    ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') !== null) {
        this.cashSumSearch.fundNo = this.route.snapshot.paramMap.get('id');
    } else {
        this.cashSumSearch.fundNo = '';
        this.cashSumSearch.year = new Date().getFullYear().toString();
    }
    this.findCashSum();
  }

  search(): void {
    this.findCashSum();
  }

  findCashSum(): void {
    this.cashService.getCashSum(this.cashSumSearch.year, this.cashSumSearch.fundNo)
      .subscribe(cashSum =>
        {
          this.cashSum = cashSum.cashFlows;
          this.cashSum.forEach(element => console.log(element));
        });
  }

  // ↑のcashSumの中で、差の合計を計算して格納する方針に変更する

  getDiff(profitPreTax: number, actualProfitPreTax: number): number {
    if (actualProfitPreTax !== null) {
      return actualProfitPreTax - profitPreTax;
    } else {
      return 0;
    }
  }

}
