import { Component, OnInit, Inject, LOCALE_ID, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CashListSearch } from '../class/cash-list-search';
import { CashService } from '../service/cash.service';
import { Cash } from '../class/cash';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-cash-list',
  templateUrl: './cash-list.component.html',
  styleUrls: ['./cash-list.component.css']
})
export class CashListComponent implements OnInit {

  title = 'キャッシュフロー一覧';
  cash: Cash = new Cash();
  cashListSearch: CashListSearch = new CashListSearch();
  displayedColumns = ['fundNo', 'fundNm', 'receiveDate', 'companyCd', 'receiveAmount'
                      , 'actualReceiveAmount', 'profitAmount', 'actualProfitAmount'];
  dataSource: MatTableDataSource<Cash>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private cashService: CashService,
    @Inject(LOCALE_ID) private locale: string,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') !== null) {
      this.cashListSearch.fundNo = this.route.snapshot.paramMap.get('id');
    }
    this.findCashList();
  }

  search(): void {
    this.findCashList();
  }

  findCashList(): void {
    this.cashService.getCashList(this.cashListSearch).subscribe(cashList => {
      this.dataSource = new MatTableDataSource(cashList.cashFlows);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  navigate(cash: Cash) {
    const convDate = formatDate(cash.receiveDate, 'yyyy/MM/dd', this.locale);
    this.router.navigate(['/cash/update'],
    { queryParams: { fundNo: cash.fundNo, receiveDate: convDate, cashFlowType: cash.cashFlowType } });
  }

}
